<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use DateTimeImmutable;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;

class ContactController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(
        ContactRepository $contacts,
        PaginatorInterface $paginator,
        Request $request
    ): Response
    {
        $contactsQuery = $contacts->findForPagination();
        $pagination = $paginator->paginate(
            $contactsQuery,
            $request->query->getInt('page', 1),
            Contact::CONTACT_PAGE_LIMIT
        );

        return $this->render(
            'contact/index.html.twig',
            [
                'paginatedContacts' => $pagination
            ]
        );
    }

    #[Route('/{slug}', name: 'app_edit')]
    public function edit(
        Request $request,
        string $slug,
        SluggerInterface $slugger,
        ContactRepository $contacts
    ): Response
    {
        $form = $this->createForm(ContactType::class, $contacts->findOneBy(['slug' => $slug]));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newContact = $form->getData();
            $newContact->setSlug(
                $slugger->slug($newContact->getName().' '.$newContact->getSurname())
            );
            
            $contacts->add($newContact, true);

            $this->addFlash('success', 'Kontakt upraven.');
            return $this->redirectToRoute('app_index');
        }

        return $this->renderForm(
            'contact/edit.html.twig',
            [
                'form' => $form,
                'contact' => $contacts->findOneBy(['slug' => $slug])
            ]
        );
    }

    #[Route('/pridat', name: 'app_add', priority: 2)]
    public function add(Request $request, SluggerInterface $slugger, ContactRepository $contacts): Response
    {
        $form = $this->createForm(ContactType::class, new Contact());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newContact = $form->getData();
            $newContact->setCreatedAt(new DateTimeImmutable());
            $newContact->setSlug(
                $slugger->slug($newContact->getName().' '.$newContact->getSurname())
            );
            
            $contacts->add($newContact, true);

            $this->addFlash('success', 'Nový kontakt vytvořen.');
            return $this->redirectToRoute('app_index');
        }

        return $this->renderForm(
            'contact/new.html.twig',
            [
                'form' => $form
            ]
        );
    }

    #[Route('/smazat/{id}', name: 'app_remove', priority: 3)]
    public function remove(int $id, ContactRepository $contacts): Response
    {
        $contact = $contacts->find($id);
        $contacts->remove($contact, true);

        $this->addFlash('success', 'Kontakt smazán.');

        return $this->redirectToRoute('app_index');
    }

    #[Route('/find-note', name: 'app_ajax_find_note', methods: ['POST'], priority: 4 )]
    public function handleFindNote(Request $request, ContactRepository $contacts): JsonResponse
    {
        $parameters = json_decode($request->getContent(), true);
        $contact = $contacts->find($parameters['id']);

        return new JsonResponse([
            'note' => $contact->getNote(),
            'name' => $contact->getName() . ' ' . $contact->getSurname()
        ]);
    }
}

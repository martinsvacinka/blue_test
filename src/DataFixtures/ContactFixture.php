<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\String\Slugger\SluggerInterface;

class ContactFixture extends Fixture
{
    /** @var Generator */
    protected $faker;

    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create('cs_CZ');
        for ($i = 0; $i < 100; ++$i) {

            $contact = new Contact;
            $contact->setName($this->faker->firstName);
            $contact->setSurname($this->faker->lastName);
            $contact->setSlug($this->slugger->slug($contact->getName().' '.$contact->getSurname()));
            $contact->setPhone($this->faker->phoneNumber);
            $contact->setEmail($this->faker->email);
            $contact->setNote($this->faker->realText(400));
            $contact->setCreatedAt(new DateTimeImmutable());
            $manager->persist($contact);
       }

       $manager->flush();
    }
}

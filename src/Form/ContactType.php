<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextAreaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Jméno'])
            ->add('surname', TextType::class, ['label' => 'Přijmení'])
            ->add('phone', TextType::class, ['label' => 'Telefon'])
            ->add('email', EmailType::class, ['label' => 'E-mail'])
            ->add('note', TextAreaType::class, [
                'label' => 'Poznámka',
                'sanitize_html' => true,
                'attr' => ['rows' => 10]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}

function _Contact()
{
	let init = function() {
		if (document.readyState !== 'loading'){
			callAfterDomLoaded();
		} else {
			document.addEventListener('DOMContentLoaded', callAfterDomLoaded);
		}
	}

	let callAfterDomLoaded = function() {
        
        bindActionShowNote();
    }
    
    const bindActionShowNote = function() {        

        let elements = document.querySelectorAll('.js_show-note');

        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', handleNoteModal, false);
        }
    }

    const handleNoteModal = function(e) {
        e.preventDefault();

        let id = e.target.dataset.id;

        fetch('/find-note', {
            body: JSON.stringify({
                id: id
            }),
            method: 'POST'
        })
        .then(response => response.json())
        .then(json => {
            showModalWithContent(json.note, json.name);
        })
    }

    const showModalWithContent = function(content, name)
    {
        // note text
        document.getElementById('js_note-modal-content').innerHTML = content;
        
        // title name
        document.getElementById('js_note-modal-name').innerHTML = name;

        let noteModal = new bootstrap.Modal(document.getElementById('noteModal'), {})
        noteModal.show();
    }

    return {
		init: init
	}
}

const Contact = new _Contact();
Contact.init();

export default Contact;

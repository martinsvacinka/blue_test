# PRIKLAD - MARTIN SVACINKA
## Spusteni

1. Spustit `docker compose build --pull --no-cache` pro prvni build images
2. `docker compose up`
3. (V root adresari projektu) `docker-compose exec -it php bin/console doctrine:fixtures:load` pro nacteni dummy dat do db

## Credits

Upravene prostredi Symfony docker vychazi z:

[https://github.com/dunglas/symfony-docker/](https://github.com/dunglas/symfony-docker/)

Created by [Kévin Dunglas](https://dunglas.fr), co-maintained by [Maxime Helias](https://twitter.com/maxhelias) and sponsored by [Les-Tilleuls.coop](https://les-tilleuls.coop).
